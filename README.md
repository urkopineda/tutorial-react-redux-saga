# React + Redux + Sagas simple tutorial

This is a simple tutorial of React using Redux and Sagas. You can find this tutorial deployed in [GitLab Pages](https://tentu.gitlab.io/tutorial-react-redux-saga/). Follow the next steps to run it:

- `npm install`
- `npm run start`

Take a look to the console logs for more information about the tutorial.
