import {
  all,
  call,
  put,
  takeEvery,
} from 'redux-saga/effects';

import {
  types,
  testSuccess,
  testFailure
} from '../actions';

import {
  httpService,
} from '../services';

/**
 * Inital saga.
 */
function* testAppSaga({url, cors}) {
  try {
    console.log(`SAGA: Requesting URL ${url}${(cors) ? ' using CORS anywhere' : ''}`)
    let data = yield call(httpService.get, {
      url,
      cors
    });
    yield put(testSuccess(data));
  } catch (error) {
    yield put(testFailure(error));
  }
}

/**
 * Export saga.
 */
export default function* testMainSaga() {
  yield all([
    takeEvery(types.TEST.HTTP.REQUEST, testAppSaga),
  ]);
}
