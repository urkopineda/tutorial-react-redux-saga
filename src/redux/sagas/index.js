import {
  all,
  fork,
} from 'redux-saga/effects';

import test from './test';

/**
 * Main saga that contains all sagas on this directory.
 */
export default function* mainSaga() {
  yield all([
    fork(test),
  ]);
}
