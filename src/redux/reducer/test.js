import {
  types,
} from '../actions';

const testState = {
  loading: false,
  success: false,
  error: false,
  data: undefined,
  url: undefined
};

export default function testReducer(
  state = testState,
  action = {}
) {
  switch (action.type) {
  case types.TEST.HTTP.REQUEST:
    console.info('REDUX: Request')
    return {
      ...state,
      loading: true,
      success: false,
      error: false,
      url: action.url
    };
  case types.TEST.HTTP.SUCCESS:
    console.info('REDUX: Success')    
    return {
      ...state,
      loading: false,
      success: true,
      error: false,
      data: action.data
    };
  case types.TEST.HTTP.FAILURE:
    console.error('REDUX: Error - ' + action.error.toString())    
    return {
      ...state,
      loading: false,
      success: false,
      error: true,
      url: undefined,
      data: undefined
    };
  default:
    return state;
  }
}
