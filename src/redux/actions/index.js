export const types = {
  TEST: {
    HTTP: {
      REQUEST: 'TEST.HTTP.REQUEST',
      SUCCESS: 'TEST.HTTP.SUCCESS',
      FAILURE: 'TEST.HTTP.FAILURE'
    }
  }
};

/* DEMO */
export const testRequest = (url, cors) => ({
  type: types.TEST.HTTP.REQUEST,
  url,
  cors
});

export const testSuccess = (data) => ({
  type: types.TEST.HTTP.SUCCESS,
  data
});


export const testFailure = (error) => ({
  type: types.TEST.HTTP.FAILURE,
  error
});
