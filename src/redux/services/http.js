const get = ({url, cors}) =>
  new Promise((resolve, reject) =>
    fetch((cors) ? `https://cors-anywhere.herokuapp.com/${url}` : url, {
      method: 'GET',
      headers: {
        origin: `${window.location.protocol}/${window.location.host}`,
      },
    })
      .then((response) => response.text())
      .catch((error) => reject(error))
      .then((text) => resolve(text))
      .catch((error) => reject(error))
  )

export default {
  get
};
