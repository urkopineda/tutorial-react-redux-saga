import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import {
  connect,
} from 'react-redux';

import {
  testRequest
} from './redux/actions';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: '',
      cors: true
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    return null;
  }

  handleInputChange = (event) => {
    console.info('COMPONENT: Changing URL to: ' + event.target.value)
    this.setState({
      url: event.target.value
    })
  }

  handleCheckboxChange = () => {
    const {
      cors
    } = this.state;
    console.info('COMPONENT: Changing CORS to: ' + !cors)
    this.setState({
      cors: !cors
    })
  }

  handleFormSubmit = (event) => {
    const {
      testRequest
    } = this.props;
    const {
      url,
      cors
    } = this.state;
    event.preventDefault();
    console.info('COMPONENT: Submiting form...');
    testRequest(url, cors);
  } 

  render() {
    const {
      url
    } = this.state;

    const {
      url: propUrl,
      loading,
      success,
      error,
      data
    } = this.props;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            React + Redux + Saga
          </p>
        </header>
        <h2>Type an URL to request it</h2>
        <form onSubmit={this.handleFormSubmit}>
          <p>
            <input type='text'
              placeholder='Write a valid URL here...'
              value={url}
              onChange={this.handleInputChange} />
            <button type='submit'>
              GET
            </button>
          </p>
          <p>
            <input type='checkbox'
              defaultChecked
              onChange={this.handleCheckboxChange} />
            <abbr title='CORS anywhere is a service that makes HTTP requests that resolves problems with Cross-Origin Requests'>
              Use CORS anywhere?
            </abbr>
          </p>
        </form>
        <h2>Redux data</h2>
        <table style={{
          margin: '0 auto',
          width: '50%'
        }}>
          <tbody>
            <tr>
              <th>URL</th>
              <td>{propUrl}</td>
            </tr>
            <tr>
              <th>Loading</th>
              <td>{(loading) ? 'TRUE' : 'FALSE'}</td>
            </tr>
            <tr>
              <th>Success</th>
              <td>{(success) ? 'TRUE' : 'FALSE'}</td>
            </tr>
            <tr>
              <th>Error</th>
              <td>{(error) ? 'TRUE' : 'FALSE'}</td>
            </tr>
          </tbody>
        </table>
        <iframe title='data' style={{
          margin: '20px',
          width: '75%',
          height: '700px'
        }} srcDoc={data} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  url: state.test.url,
  data: state.test.data,
  loading: state.test.loading,
  success: state.test.success,
  error: state.test.error
});

const mapDispatchToProps = {
  testRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
